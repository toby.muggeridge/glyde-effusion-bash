#!/bin/bash

### INITIALIZE ###
  # Install programs the installer needs.
    sudo apt install -y curl wget lsb-release ipcalc bind9-utils net-tools neofetch

  # Enable local UNBOUND resolution so script can succeed.
    #sudo systemctl enable systemd-resolved.service

  # Setup colors for the installer.
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    BLUE='\033[0;34m'
    RED='\033[0;31m'
    NC='\033[0m'

    rnservername="SOHO-ROM-MDF-ACS1"
      # @TODO code to get ACS name from host provided input.
  sudo sh -c 'echo "SOHO-ROM-MDF-ACS1" > /etc/motd'

### DO CLEANUP SO REINSTALL OF SCRIPT DOES NOT FAIL ###
  # Stop, Uninstall and Purge NGINX and Files.
    echo -ne "${YELLOW}PURGING NGINX${NC}\n"
    sudo sleep 1
    sudo systemctl stop nginx
    sudo apt remove --purge nginx* -y
    sudo apt autoremove -y
    sudo rm -rf /usr/share/nginx
  # Stop, Uninstall and Purge UNBOUND and Files.
    echo -ne "${YELLOW}PURGING UNBOUND${NC}\n"
    sudo sleep 1
    sudo systemctl stop unbound
    sudo apt remove --purge unbound* -y
    sudo apt autoremove -y
    sudo rm -rf /etc/unbound
  # Stop, Uninstall and Purge RSYNC and Files.
    echo -ne "${YELLOW}PURGING RSYNC${NC}\n"
    sudo sleep 1
    sudo apt remove --purge rsync* -y
    sudo apt autoremove -y

  # Disable and Remove services so we can replace them further along in the script.

    # Enable Services
      echo "Disabling services..."
      echo "Disabling NGINX Service"
      sudo systemctl disable nginx
      sleep 1
      echo "Disabling UNBOUND Service"
      sudo systemctl disable unbound
      sleep 1
      echo "Disabling GES Service"
      sudo systemctl disable ges
      sleep 1
      echo "Disabling GES ACS Service"
      sudo systemctl disable ges_acs
      sleep 1
      echo "Disabling GES NGINX Service"
      sudo systemctl disable ges_nginx
      sleep 1
      echo "Disabling GES RSYNC Service"
      sudo systemctl disable ges_rsync
      sleep 1
      echo "Disabling GES UNBOUND Service"
      sudo systemctl disable ges_unbound
      sleep 1

      echo "Stopping services..."
      echo ""
      echo "Stopping NGINX Service"
      sudo systemctl stop nginx
      sleep 1
      echo "Stopping UNBOUND Service"
      sudo systemctl stop unbound
      sleep 1
      echo "Stopping GES Service"
      sudo systemctl stop ges
      sleep 1
      echo "Stopping GES ACS Service"
      sudo systemctl stop ges_acs
      sleep 1
      echo "Stopping GES NGINX Service"
      sudo systemctl stop ges_nginx
      sleep 1
      echo "Stopping GES RSYNC Service"
      sudo systemctl stop ges_rsync
      sleep 1
      echo "Stopping GES UNBOUND Service"
      sudo systemctl stop ges_unbound
      sleep 1

      # Reload the systemd daemon
        echo "Reloading systemd daemon"
        sudo systemctl daemon-reload

      echo "Done"