#!/bin/bash

# Prompt the user for network configuration details
read -p "Enter the IPv4 address (e.g., 10.10.20.19/24): " ipv4_address
read -p "Enter the gateway address: " gateway
read -p "Enter DNS server addresses (space-separated, e.g., 8.8.8.8 9.9.9.9): " dns_servers

# Split the DNS servers into an array
IFS=' ' read -ra dns_array <<< "$dns_servers"

# Create or update the Netplan YAML file for enp0s8
cat > /etc/netplan/00-installer-config.yaml<<EOF
network:
  ethernets:
    enp0s1:
      dhcp4: true
    enp0s8:
      addresses:
        - $ipv4_address
      gateway4: $gateway
      nameservers:
        addresses:
$(for dns in "${dns_array[@]}"; do
    echo "          - $dns"
done)
        search: []
  version: 2
EOF

# Apply the Netplan configuration
netplan apply

echo "Netplan configuration for enp0s8 updated!"

#!/bin/bash

# Prompt the user for network configuration details
read -p "Enter the IPv4 address (e.g., 10.10.20.19/24): " ipv4_address
read -p "Enter the gateway address: " gateway
read -p "Enter DNS server addresses (space-separated, e.g., 8.8.8.8 9.9.9.9): " dns_servers

# Split the DNS servers into an array
IFS=' ' read -ra dns_array <<< "$dns_servers"

# Create or update the Netplan YAML file for enp0s8
cat > /etc/netplan/01-netcfg.yaml <<EOF
network:
  ethernets:
    enp0s1:
      dhcp4: true
    enp0s8:
      addresses:
        - $ipv4_address
      gateway4: $gateway
      nameservers:
        addresses:
$(for dns in "${dns_array[@]}"; do
    echo "          - $dns"
done)
        search: []
  version: 2
EOF

# Apply the Netplan configuration
netplan apply

echo "Netplan configuration for enp0s8 updated!"

#!/bin/bash

# Default DNS servers
dns_servers="8.8.8.8 9.9.9.9"

# Prompt the user for network configuration details
read -p "Enter the IPv4 address (e.g., 10.10.20.19/24): " ipv4_address
read -p "Enter the gateway address: " gateway

# Create or update the Netplan YAML file for enp0s8
cat > /etc/netplan/00-installer-config.yaml <<EOF
network:
  ethernets:
    enp0s1:
      dhcp4: true
    enp0s8:
      addresses:
        - $ipv4_address
      gateway4: $gateway
      nameservers:
        addresses:
$(for dns in $dns_servers; do
    echo "          - $dns"
done)
        search: []
  version: 2
EOF

# Apply the Netplan configuration
netplan apply

echo "Netplan configuration for enp0s8 updated!"

