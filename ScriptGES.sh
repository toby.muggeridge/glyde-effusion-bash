#!/bin/bash

function ProgressBarPRTG {
# Process data
	let _progress=(${1}*100/${2}*100)/100
	let _done=(${_progress}*4)/10
	let _left=40-$_done
# Build progressbar string lengths
	_done=$(printf "%${_done}s")
	_left=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:
# 1.2.1.1 Progress : [########################################] 100%
printf "\rPosting PRTG Data : [${_done// /#}${_left// /-}] ${_progress}%%"

}

# START OF GES FUNCTION
function init(){
    # VM IP Address
			# Get IP Address ex: 192.168.1.193
			ges_vm_ip_address=$(ip addr | grep -oP 'inet \K[\d.]+' | grep -v '127.0.0.1' | grep -v '10.0.2.15')
			# Get IP Address w/subnet ex: 192.168.1.193/24
			ges_vm_ip_address_w_subnet=$(ip -o -f inet addr show | awk '/scope global/ {print $4}' | grep -v '10.0.2.15/24')
			# Get Network Address ex: 192.168.1.0/24
			ges_vm_ip_network=$(ipcalc -n $(ip -o -f inet addr show dev enp0s2 | awk '/inet/ {print $4}') | grep -o 'Network:\s*[0-9./]*' | awk '{print $2}')

		# Check UNBOUND IP Address
			# Check access-control ip

			# Check interface ip
				#interface_ip_string_expected='local-data: "king.roomnet.com A '$ges_vm_ip_address_found'"'
				interface_ip_string_found=$(grep "king.roomnet.com" /etc/unbound/unbound.conf.d/rn_unbound.conf)
				echo $interface_ip_string_found
				if [[ $interface_ip_string_found == *$ges_vm_ip_address* ]]; then
				  echo "IP Address Matches"
					sudo echo $ges_vm_ip_address > /usr/share/nginx/index.html
					# Do Nothing
				else
					echo "IP Does Not Match"
					# Do Something, in this case we will create an accurate UNBOUND conf file, copy into place and restart UNBOUND.
						sudo rm -f /etc/unbound/unbound.conf.d/rn_unbound.conf
				    sudo cp /home/ges/rn_unbound_blocked /etc/unbound/unbound.conf.d/rn_unbound.conf
				    sudo chown root:root /etc/unbound/unbound.conf.d/rn_unbound.conf
				    sudo chmod 644 /etc/unbound/unbound.conf.d/rn_unbound.conf
						# Replaced forward slash with pipe as the delimiter because the forward slash in the subnet confused sed.
						sudo sed -i.bak "s|IPADDRESS|$ges_vm_ip_address|g; s|IPNETWORK|$ges_vm_ip_network|g" /etc/unbound/unbound.conf.d/rn_unbound.conf
						sudo systemctl restart unbound

						sudo echo $ges_vm_ip_address > /usr/share/nginx/index.html
				fi

    # REGENERATE NETWORK


# TEST FUNCTION


    initPRTG
}


function initPRTG(){

    # Current Time Stamp
    timestamp=$(date +%m/%d/%Y-%H:%M:%S)

echo -e "\e[36m                                                              \e[0m";
echo -e "\e[36m                                                              \e[0m";
echo -e "\e[36m        GGGGGGGGGGGGGEEEEEEEEEEEEEEEEEEEEEE   SSSSSSSSSSSSSSS \e[0m";
echo -e "\e[36m     GGG::::::::::::GE::::::::::::::::::::E SS:::::::::::::::S\e[0m";
echo -e "\e[36m   GG:::::::::::::::GE::::::::::::::::::::ES:::::SSSSSS::::::S\e[0m";
echo -e "\e[36m  G:::::GGGGGGGG::::GEE::::::EEEEEEEEE::::ES:::::S     SSSSSSS\e[0m";
echo -e "\e[36m G:::::G       GGGGGG  E:::::E       EEEEEES:::::S            \e[0m";
echo -e "\e[36mG:::::G                E:::::E             S:::::S            \e[0m";
echo -e "\e[36mG:::::G                E::::::EEEEEEEEEE    S::::SSSS         \e[0m";
echo -e "\e[36mG:::::G    GGGGGGGGGG  E:::::::::::::::E     SS::::::SSSSS    \e[0m";
echo -e "\e[36mG:::::G    G::::::::G  E:::::::::::::::E       SSS::::::::SS  \e[0m";
echo -e "\e[36mG:::::G    GGGGG::::G  E::::::EEEEEEEEEE          SSSSSS::::S \e[0m";
echo -e "\e[36mG:::::G        G::::G  E:::::E                         S:::::S\e[0m";
echo -e "\e[36m G:::::G       G::::G  E:::::E       EEEEEE            S:::::S\e[0m";
echo -e "\e[36m  G:::::GGGGGGGG::::GEE::::::EEEEEEEE:::::ESSSSSSS     S:::::S\e[0m";
echo -e "\e[36m   GG:::::::::::::::GE::::::::::::::::::::ES::::::SSSSSS:::::S\e[0m";
echo -e "\e[36m     GGG::::::GGG:::GE::::::::::::::::::::ES:::::::::::::::SS \e[0m";
echo -e "\e[36m        GGGGGG   GGGGEEEEEEEEEEEEEEEEEEEEEE SSSSSSSSSSSSSSS   \e[0m";
echo -e "\e[36m                                                              \e[0m";
echo -e "\e[36m-----------------GLYDE EFFUSION SERVER STATUS-----------------\e[0m";
echo -e "\e[36m                      $timestamp                              \e[0m";
echo -e "\e[36m                                                              \e[0m";

# LIVE STATUS HEADER
    echo ""
# Apple Caching Server Status...
    StatusACS=$(</home/ges/StatusACS.txt)
    echo "ACS: $StatusACS"

# Check UNBOUND Status...
    StatusUNBOUND=$(</home/ges/StatusUNBOUND.txt)
    echo "UNBOUND: $StatusUNBOUND"

# Check NGINX Status...
    StatusNGINX=$(</home/ges/StatusNGINX.txt)
    echo "NGINX: $StatusNGINX"

# RSYNC Status...
    StatusRSYNC=$(</home/ges/StatusRSYNC.txt)
    echo "RSYNC: $StatusRSYNC"

# GES VERSION
    FILE=/home/ges/ges.version
    if [ -f "$FILE" ]; then
        GESversion=$(</home/ges/ges.version)
        echo "GES VERSION: $GESversion"
    else
        GESversion="0"
        echo "GES VERSION: Unknown"
    fi

# SITE UUID
	FILE=/home/ges/site.uuid
    if [ -f "$FILE" ]; then
		siteUUID=$(</home/ges/site.uuid)
		echo "SITE UUID: $siteUUID"
        echo ""
	else
		siteUUID=00000000-0000-0000-0000-000000000000
		echo "SITE UUID: $siteUUID"
        echo ""
    fi

# PRTG UUID
	FILE=/home/ges/prtg.uuid
    if [ -f "$FILE" ]; then
		prtgUUID=$(</home/ges/prtg.uuid)
		echo "PRTG UUID: $prtgUUID"
        echo ""
	else
		prtgUUID=00000000-0000-0000-0000-000000000000
		echo "PRTG UUID: $prtgUUID"
        echo ""
    fi



		# FALLBACK TO RAISE PRTG ALERT IF CAN NOT GET ALERT FROM SINGLE CHANNEL BEING DOWN BY SETTING ALL CHANNELS TO 0 ON ANY FAILURES.
	if [[ "$StatusUNBOUND" == "1" && "$StatusNGINX" == "1" && "$StatusRSYNC" == "1" ]]; then
		StatusACS=$StatusACS
		StatusUNBOUND=$StatusUNBOUND
		StatusNGINX=$StatusNGINX
		StatusRSYNC=$StatusRSYNC
		GESversion=$GESversion
    else
		StatusACS=0
		StatusUNBOUND=0
		StatusNGINX=0
		StatusRSYNC=0
		GESversion=0
    fi

# Calling ProgressBarPRTG function...
	# Variables
	_start=1
	# This accounts as the "totalState" variable for the ProgressBarPRTG function in seconds
	_end=10
	# Proof of concept
	for number in $(seq ${_start} ${_end})
	do
	sleep 1.0
	ProgressBarPRTG ${number} ${_end}
	done


		prtgSEND
}

prtgSEND(){
    echo ""
		echo ""
		echo ""
    echo -e "\e[1;35;4m--------------------------PRTG RESULT--------------------------\e[0m"
		# Apple Caching Server Status...
		    echo "ACS: $StatusACS"

		# Check UNBOUND Status...
		    echo "UNBOUND: $StatusUNBOUND"

		# Check NGINX Status...
		    echo "NGINX: $StatusNGINX"

		# RSYNC Status...
		    echo "RSYNC: $StatusRSYNC"

		# GES VERSION
		    echo "GES VERSION: $GESversion"
    echo ""
    generate_post_data()
{
  cat <<EOF
{
    "prtg": {
        "result": [{
                "Channel": "Webserver",
                "Unit": "Count",
                "Value": "$StatusNGINX",
                "VolumeSize": "One",
                "Mode": "Absolute",
                "LimitMode": 1,
                "LimitMinError": 0
            },
            {
                "Channel": "DNS",
                "Unit": "Count",
                "Value": "$StatusUNBOUND",
                "VolumeSize": "One",
                "Mode": "Absolute",
                "LimitMode": 1,
                "LimitMinError": 0
            },
            {
                "Channel": "Apple Cache",
                "Unit": "Count",
                "Value": "$StatusACS",
                "VolumeSize": "One",
                "Mode": "Absolute",
                "LimitMode": 1,
                "LimitMinError": 0
            },
            {
                "Channel": "CDN Sync",
                "Unit": "Count",
                "Value": "$StatusRSYNC",
                "VolumeSize": "One",
                "Mode": "Absolute",
                "LimitMode": 1,
                "LimitMinError": 0
            },
            {
                "Channel": "GES Version",
                "Unit": "Custom",
                "CustomUnit": "v",
                "Value": "$GESversion",
                "VolumeSize": "One",
                "Mode": "Absolute",
                "LimitMode": 1,
                "LimitMinError": 0
            }
        ]
    }
}
EOF
}



curl --insecure -i \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-X POST --data "$(generate_post_data)" "https://us-west-monitor1.roomnet.com:5051/$prtgUUID"
		echo ""
		echo ""
		    # Current Time Stamp
    timestamp=$(date +%Y-%m-%d_%H-%M-%S)
		#echo -e "\e[1;35;4m**********************************PRTG END********************************\e[0m"
		echo ""
}

# Run PRTG Sensor, results are only deplayed 1 minute by cron running this script. Cleaner output when viewing and all status checks collectively completed before being sent.
init



exit 0
