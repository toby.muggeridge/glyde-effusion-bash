        echo "Restart services..."
        echo ""
        echo "Restart NGINX Service"
        sudo systemctl restart nginx
        sleep 1
        echo "Restart UNBOUND Service"
        sudo systemctl restart unbound
        sleep 1
        echo "Restart GES Service"
        sudo systemctl restart ges
        sleep 1
        echo "Restart GES ACS Service"
        sudo systemctl restart ges_acs
        sleep 1
        echo "Restart GES NGINX Service"
        sudo systemctl restart ges_nginx
        sleep 1
        echo "Restart GES RSYNC Service"
        sudo systemctl restart ges_rsync
        sleep 1
        echo "Restart GES UNBOUND Service"
        sudo systemctl restart ges_unbound
        sleep 1

        echo "Done"