#!/bin/bash

function ProgressBarACS {
# Process data
	let _progress=(${1}*100/${2}*100)/100
	let _done=(${_progress}*4)/10
	let _left=40-$_done
# Build progressbar string lengths
	_done=$(printf "%${_done}s")
	_left=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:
# 1.2.1.1 Progress : [########################################] 100%
printf "\rRESTARTING ACS : [${_done// /#}${_left// /-}] ${_progress}%%"

}

	# ACS Host IP Address Value
function acsHostSTATUS(){
    echo $(</home/ges/acs.status)
}

	# Check ACS status by polling acs file from host.
function testACS(){
   echo $(dig +tries=1 +time=2 king.roomnet.com @$(acsHostIP) | grep 'SERVER' | cut -d ' ' -f3)
}

function initACS(){
    if [ "$(acsHostSTATUS)" == "1" ]; then
			echo -e "\e[1;32;4m************************************ACS PASS************************************\e[0m"
			echo "1" > /home/ges/StatusACS.txt

		# Set back to 0 for positive ACS Status of 1 from ACS host!
		echo "0" > /home/ges/acs.status
    else
			echo -e "\e[1;31;4m************************************ACS FAIL************************************\e[0m"
			echo "0" > /home/ges/StatusACS.txt

    fi
}

initACS

exit 0
