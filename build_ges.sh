#!/bin/bash

# Constants for text colors
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RED='\033[0;31m'
NC='\033[0m'

# Prompt the user for the device name and set it as rnservername
read -p "Enter the device name (ORG-SITE-LOCATION-DEVICE): " rnservername

# Prompt the user for the site UUID and create a file called site.uuid
read -p "Enter the site UUID: " site_uuid
echo "$site_uuid" > site.uuid

# Set the ACS name in /etc/motd
sudo sh -c "echo '$rnservername' > /etc/motd"

# Function to install packages
install_packages() {
    echo -ne "${YELLOW}INSTALLING $1${NC}\n"
    sudo sleep 1
    sudo apt install "$1" -y
}

# Function to stop and purge services
stop_and_purge_service() {
    echo -ne "${YELLOW}PURGING $1${NC}\n"
    sudo sleep 1
    sudo systemctl stop "$1"
    sudo apt remove --purge "$1"* -y
    sudo apt autoremove -y
}

# Function to disable and stop services
disable_and_stop_service() {
    echo "Disabling $1 Service"
    sudo systemctl disable "$1"
    sleep 1
    echo "Stopping $1 Service"
    sudo systemctl stop "$1"
    sleep 1
}

# Install required packages
sudo apt install -y curl wget lsb-release ipcalc bind9-utils net-tools neofetch

# Clean up NGINX, UNBOUND, and RSYNC
stop_and_purge_service "nginx"
stop_and_purge_service "unbound"
stop_and_purge_service "rsync"

# Disable and stop services
services_to_disable=("nginx" "unbound" "ges" "ges_acs" "ges_nginx" "ges_rsync" "ges_unbound")
for service in "${services_to_disable[@]}"; do
    disable_and_stop_service "$service"
done

# Update APT for package installation
echo -ne "${YELLOW}UPDATING APT${NC}\n"
sudo sleep 1
sudo apt update -y

# Set hostname
sudo hostnamectl set-hostname "king.roomnet.com"
getHostname=$(hostname)
echo -ne "${YELLOW}Hostname has been set to: $getHostname ${NC}\n"
sudo sleep 1

# Install NGINX, UNBOUND, and RSYNC
install_packages "nginx"
install_packages "unbound"
install_packages "rsync"

# Perform NGINX post-installation work
echo -ne "${YELLOW}NGINX POST INSTALL WORK${NC}\n"
sudo sleep 1

# Replace NGINX default site with ROOMNET specific site
nginx_conf_file="/etc/nginx/sites-available/default"
nginx_config_source="/home/ges/rn_nginx"
sudo rm -f "$nginx_conf_file"
sudo cp "$nginx_config_source" "$nginx_conf_file"
sudo chown root:root "$nginx_conf_file"
sudo chmod 644 "$nginx_conf_file"


# True King Server Pre Data Sync (CERTS etc, required to start NGINX w/SSL.)
echo -ne "${YELLOW}AWS KING SERVER WORK${NC}\n"
sudo sleep 1
# If this step fails then the installer folder needs these updated first!
sudo chown ges:ges us-east-king1.pem
sudo chmod 400 us-east-king1.pem
rsync --progress -avz -e "ssh -o StrictHostKeyChecking=no -i us-east-king1.pem" centos@us-east-king1.roomnet.com:/usr/share/nginx/server.crt /usr/share/nginx/
rsync --progress -avz -e "ssh -o StrictHostKeyChecking=no -i us-east-king1.pem" centos@us-east-king1.roomnet.com:/usr/share/nginx/server.key /usr/share/nginx/
rsync --progress -avz -e "ssh -o StrictHostKeyChecking=no -i us-east-king1.pem" centos@us-east-king1.roomnet.com:/usr/share/nginx/users /usr/share/nginx/

sudo sed -i.bak "s|Welcome to nginx!|$rnservername|g" /usr/share/nginx/html/index.html

# Restart NGINX
sudo systemctl restart nginx

# Perform UNBOUND post-installation work
echo -ne "${YELLOW}UNBOUND POST INSTALL WORK${NC}\n"
sudo sleep 1

# Overwrite unbound.conf with ROOMNET specific
unbound_conf_file="/etc/unbound/unbound.conf.d/rn_unbound.conf"
unbound_config_source="/home/ges/rn_unbound_blocked"
sudo rm -f "$unbound_conf_file"
sudo cp "$unbound_config_source" "$unbound_conf_file"
sudo chown root:root "$unbound_conf_file"
sudo chmod 644 "$unbound_conf_file"

# Set UNBOUND config to VM IP address & Subnet
# Get VM IP Address ex: 192.168.1.193
ges_vm_ip_address=$(ip addr | grep -oP 'inet \K[\d.]+' | grep -v '127.0.0.1' | grep -v '10.0.2.15')

# Get IP Address w/subnet ex: 192.168.1.193/24
ges_vm_ip_address_w_subnet=$(ip -o -f inet addr show | awk '/scope global/ {print $4}' | grep -v '10.0.2.15/24')

# Get Network Address ex: 192.168.1.0/24
ges_vm_ip_network=$(ipcalc -n $(ip -o -f inet addr show dev enp0s2 | awk '/inet/ {print $4}') | grep -o 'Network:\s*[0-9./]*' | awk '{print $2}')

# Replaced forward slash with pipe as the delimiter because the forward slash in the subnet confused sed.
sudo sed -i.bak "s|IPADDRESS|$ges_vm_ip_address|g; s|IPNETWORK|$ges_vm_ip_network|g" "$unbound_conf_file"

# Restart UNBOUND
sudo systemctl restart unbound

# Create UNBOUND Log
# touch /etc/unbound/unbound.log
# chown unbound:unbound /etc/unbound/unbound.log
# chmod 640 /etc/unbound/unbound.log

# INSTALL COMPLETE BANNER
echo -ne "${RED}Pre Installation Build Script Complete!${NC}\n"
echo -ne "${NC}Pre Installation Build Script Complete!${NC}\n"
echo -ne "${BLUE}Pre Installation Build Script Complete!${NC}\n"
echo -ne "${YELLOW}Pre Installation Build Script Complete!${NC}\n"
echo -ne "${GREEN}Pre Installation Build Script Complete!${NC}\n"
echo "--------------------------------------------------------------------------------"
echo -ne "${NC}NGINX & UNBOUND ARE NOW OPERATING CORRECTLY.${NC}\n"
echo -ne "${NC}TEST BY USING $ges_vm_ip_address AS YOUR DNS SERVER.${NC}\n"
echo -ne "${NC}GO TO https://king.roomnet.com:8443${NC}\n"
echo -ne "${NC}THERE SHOULD BE NO CERTIFICATE ERROR.${NC}\n"
echo -ne "${NC}THIS CAN NOW BE TESTED DIRECTLY ON THE MAC MINI!${NC}\n"

sleep 2

# Start RSYNC and other services as needed
echo -ne "${YELLOW}STARTING RSYNC${NC}\n"
sudo bash ScriptRSYNC.sh
sleep 1
sudo bash runServices.sh
echo -ne "${GREEN}COMPLETE!${NC}\n"

