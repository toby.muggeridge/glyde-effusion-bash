#!/bin/bash

# Function to check the number of running rsync processes
count_rsync_processes() {
    ps ax | grep rsync | grep -v grep | grep -v "$0" | wc -l
}

# Function to get the site UUID
get_site_uuid() {
    local site_uuid=00000000-0000-0000-0000-000000000000
    local file="/home/ges/site.uuid"
    
    if [ -f "$file" ]; then
        local site_uuid_file=$(<"$file")
        [ -z "$site_uuid_file" ] || site_uuid="$site_uuid_file"
    fi
    
    echo "$site_uuid"
}

# Function to perform the rsync operation
perform_rsync() {
    local site_uuid="$1"
    local timestamp=$(date +%Y-%m-%d_%H-%M-%S)
    local ssh_options="-o StrictHostKeyChecking=no -i /home/ges/us-east-king1.pem"
    local source_server="centos@us-east-king1.roomnet.com"
    local source_directory="/usr/share/nginx"
    local destination_directory="/usr/share/nginx"
    
    echo "$(count_rsync_processes) RSYNC processes running."
    echo -e "\e[1;32;4m***********************RSYNC START $timestamp*************************\e[0m"
    
    echo "SITE UUID: $site_uuid"
    
    # Perform rsync operations
    if rsync --stats --human-readable --info=progress2 -avz --delete \
    --exclude=html/sites --exclude=modules-available --exclude=html/index.html --exclude=modules \
    -e "ssh $ssh_options" "$source_server:$source_directory/" "$destination_directory/" \
    && rsync --stats --human-readable --info=progress2 -avz --delete \
    -e "ssh $ssh_options" "$source_server:$source_directory/html/sites/$site_uuid" "$destination_directory/html/sites/"; then
    echo -e "\e[1;32;4m************************RSYNC PASS $timestamp*************************\e[0m"
    echo "1" > /home/ges/StatusRSYNC.txt
else
    echo -e "\e[1;31;4m************************RSYNC FAIL $timestamp*************************\e[0m"
    echo "0" > /home/ges/StatusRSYNC.txt
fi
# ..
}

# Main function
main() {
    local siteUUID=$(get_site_uuid)
    perform_rsync "$siteUUID"
}

# Call the main function
main
