#!/bin/bash

# Setup systemd services.
  # Copy ges.service to systemd system services
      echo "Installing ges.service..."
      sudo rm -f /etc/systemd/system/ges.service
      sudo cp /home/ges/ges.service /etc/systemd/system
      sudo chown root:root /etc/systemd/system/ges.service
      sudo chmod 644 /etc/systemd/system/ges.service

  # Copy ges_acs.service to systemd system services
      echo "Installing ges_acs.service..."
      sudo rm -f /etc/systemd/system/ges_acs.service
      sudo cp /home/ges/ges_acs.service /etc/systemd/system
      sudo chown root:root /etc/systemd/system/ges_acs.service
      sudo chmod 644 /etc/systemd/system/ges_acs.service

  # Copy ges_nginx.service to systemd system services
      echo "Installing ges_nginx.service..."
      sudo rm -f /etc/systemd/system/ges_nginx.service
      sudo cp /home/ges/ges_nginx.service /etc/systemd/system
      sudo chown root:root /etc/systemd/system/ges_nginx.service
      sudo chmod 644 /etc/systemd/system/ges_nginx.service

  # Copy ges_rsync.service to systemd system services
      echo "Installing ges_rsync.service..."
      sudo rm -f /etc/systemd/system/ges_rsync.service
      sudo cp /home/ges/ges_rsync.service /etc/systemd/system
      sudo chown root:root /etc/systemd/system/ges_rsync.service
      sudo chmod 644 /etc/systemd/system/ges_rsync.service

  # Copy ges_unbound.service to systemd system services
      echo "Installing ges_unbound.service..."
      sudo rm -f /etc/systemd/system/ges_unbound.service
      sudo cp /home/ges/ges_unbound.service /etc/systemd/system
      sudo chown root:root /etc/systemd/system/ges_unbound.service
      sudo chmod 644 /etc/systemd/system/ges_unbound.service

  # Chmod the scripts
  sudo chmod +x /home/ges/ScriptGES.sh
  sudo chmod +x /home/ges/ScriptNGINX.sh
  sudo chmod +x /home/ges/ScriptACS.sh
  sudo chmod +x /home/ges/ScriptUNBOUND.sh
  sudo chmod +x /home/ges/ScriptRSYNC.sh

  # Reload the systemd daemon
    echo "Reloading systemd daemon"
    sudo systemctl daemon-reload

    # Enable Services
        echo "Enabling services..."
        echo "Enabling NGINX Service"
        sudo systemctl enable nginx
        sleep 1
        echo "Enabling UNBOUND Service"
        sudo systemctl enable unbound
        sleep 1
        echo "Enabling GES Service"
        sudo systemctl enable ges
        sleep 1
        echo "Enabling GES ACS Service"
        sudo systemctl enable ges_acs
        sleep 1
        echo "Enabling GES NGINX Service"
        sudo systemctl enable ges_nginx
        sleep 1
        echo "Enabling GES RSYNC Service"
        sudo systemctl enable ges_rsync
        sleep 1
        echo "Enabling GES UNBOUND Service"
        sudo systemctl enable ges_unbound
        sleep 1

        echo "Starting services..."
        echo ""
        echo "Starting NGINX Service"
        sudo systemctl start nginx
        sleep 1
        echo "Starting UNBOUND Service"
        sudo systemctl start unbound
        sleep 1
        echo "Starting GES Service"
        sudo systemctl start ges
        sleep 1
        echo "Starting GES ACS Service"
        sudo systemctl start ges_acs
        sleep 1
        echo "Starting GES NGINX Service"
        sudo systemctl start ges_nginx
        sleep 1
        echo "Starting GES RSYNC Service"
        sudo systemctl start ges_rsync
        sleep 1
        echo "Starting GES UNBOUND Service"
        sudo systemctl start ges_unbound
        sleep 1

        echo "Done"

