#!/bin/bash

function ProgressBarUNBOUND {
# Process data
	let _progress=(${1}*100/${2}*100)/100
	let _done=(${_progress}*4)/10
	let _left=40-$_done
# Build progressbar string lengths
	_done=$(printf "%${_done}s")
	_left=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:
# 1.2.1.1 Progress : [########################################] 100%
printf "\rRESTARTING UNBOUND : [${_done// /#}${_left// /-}] ${_progress}%%"

}


	# Get VM IP Address ex: 192.168.1.193
	ges_vm_ip_address=$(ip addr | grep -oP 'inet \K[\d.]+' | grep -v '127.0.0.1' | grep -v '10.0.2.15')
			# Get IP Address w/subnet ex: 192.168.1.193/24
			ges_vm_ip_address_w_subnet=$(ip -o -f inet addr show | awk '/scope global/ {print $4}' | grep -v '10.0.2.15/24')
			# Get Network Address ex: 192.168.1.0/24
		   ges_vm_ip_network=$(ipcalc -n $(ip -o -f inet addr show dev enp0s2 | awk '/inet/ {print $4}') | grep -o 'Network:\s*[0-9./]*' | awk '{print $2}')
	# Check UNBOUND status by testing for proper resolution.
function testUNBOUND(){
   echo $(dig +tries=1 +time=2 king.roomnet.com @$ges_vm_ip_address | grep 'SERVER' | cut -d ' ' -f3)
}

function initUNBOUND(){
    if [ "$(testUNBOUND)" == "$ges_vm_ip_address#53($ges_vm_ip_address)" ]; then
			echo -e "\e[1;32;4m**********************************UNBOUND PASS**********************************\e[0m"
        echo "1" > /home/ges/StatusUNBOUND.txt
    else
				# UNBOUND config recovery in case ScriptGES.sh missed it.
					echo "IP Does Not Match"
					sudo rm -f /etc/unbound/unbound.conf.d/rn_unbound.conf
					sudo cp /home/ges/rn_unbound_blocked /etc/unbound/unbound.conf.d/rn_unbound.conf
					sudo chown root:root /etc/unbound/unbound.conf.d/rn_unbound.conf
					sudo chmod 644 /etc/unbound/unbound.conf.d/rn_unbound.conf
					# Replaced forward slash with pipe as the delimiter because the forward slash in the subnet confused sed.
					sudo sed -i.bak "s|IPADDRESS|$ges_vm_ip_address|g; s|IPNETWORK|$ges_vm_ip_network|g" /etc/unbound/unbound.conf.d/rn_unbound.conf
					sudo systemctl restart unbound

			# Calling ProgressBarUNBOUND function...
			# Variables
			_start=1
			# This accounts as the "totalState" variable for the ProgressBarUNBOUND function in seconds
			_end=5
			# Proof of concept
			for number in $(seq ${_start} ${_end})
			do
				sleep 1.0
				ProgressBarUNBOUND ${number} ${_end}
			done

			echo -e "\e[1;31;4m**********************************UNBOUND FAIL**********************************\e[0m"
			echo "0" > /home/ges/StatusUNBOUND.txt

	initUNBOUND

    fi
}

initUNBOUND

exit 0
