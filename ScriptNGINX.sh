#!/bin/bash

function ProgressBarNGINX {
    # Process data
    local _progress=$((($1 * 100) / $2))
    local _done=$(($_progress * 4 / 10))
    local _left=$((40 - $_done))
    
    # Build progressbar string lengths
    local _done_bar=$(printf "%${_done}s")
    local _left_bar=$(printf "%${_left}s")
    
    # Build and print the ProgressBar line
    printf "\rRESTARTING NGINX : [${_done_bar// /#}${_left_bar// /-}] ${_progress}%%"
}

function testNGINX() {
    local response_code=$(curl --insecure -I https://$ges_vm_ip_address:8443 | grep 'HTTP' | cut -d ' ' -f2)
    echo "$response_code"
}

function restartNGINX() {
    systemctl restart nginx
}

function main() {
    ges_vm_ip_address=$(ip addr | grep -oP 'inet \K[\d.]+' | grep -v '127.0.0.1' | grep -v '10.0.2.15')
    
    if [ "$(testNGINX)" == "200" ]; then
        echo -e "\e[1;32;4m***********************************NGINX PASS***********************************\e[0m"
        echo "1" > /home/ges/StatusNGINX.txt
    else
        restartNGINX
        
        # Restarting NGINX with progress bar
        for number in {1..5}; do
            sleep 1
            ProgressBarNGINX $number 5
        done
        echo ""
        
        echo -e "\e[1;31;4m***********************************NGINX FAIL***********************************\e[0m"
        echo "0" > /home/ges/StatusNGINX.txt
    fi
}

main

